#----------------------------------------------------------------- importing libraries

from math import *
import matplotlib.pyplot as plt 
import datetime
import matplotlib.animation as animation

#----------------------------------------------------------------- varuables

sec_p = 18 #second pointer var
min_p = 18 #minute pointer var
hour_p = 10 #hour pointer var
marker_r = 22
cxy = 30 #clock radius var

fig = plt.figure() #naming a figure 
ax = fig.add_subplot( 111 ) #adding subplot

#----------------------------------------------------------------- #calculating some shapes cords on our clock for easier undrestanding

def marker():
    x = []
    y = []
    a = []
    b = []
    
    for i in range( 0 , 360 , 30 ):
        x.append( cxy + marker_r * cos( radians( i ) ) )
        y.append( cxy + marker_r * sin( radians( i ) ) )
    
    for i in range( 0 , 360 , 6 ):
        a.append( cxy + marker_r * cos( radians( i ) ) )
        b.append( cxy + marker_r * sin( radians( i ) ) )
    return x , y , a , b


#----------------------------------------------------------------- #calculating the second spot using cos() and sin()

def calculate():
    t = datetime.datetime.now()                                 
    h = t.hour #hour var
    m = t.minute #minute var
    s = t.second #second var

    #finding the second spot for the second pointer 

    deg_s = ( 15 - s ) * 6 
    xs = [ cxy , cxy + ( sec_p * cos( radians( deg_s ) ) ) ] 
    ys = [ cxy , cxy + ( sec_p * sin( radians( deg_s ) ) ) ] 

    #finding the second spot for the minute pointer 

    deg_m = ( 15 - m ) * 6
    xm = [ cxy , cxy + ( min_p * cos( radians( deg_m ) ) ) ] 
    ym = [ cxy , cxy + ( min_p * sin( radians( deg_m ) ) ) ] 

    #finding the second spot for the hour pointer 

    deg_h = ( ( 3 - ( h % 23 ) ) * 30 ) - ( m / 2 )
    xh = [ cxy , cxy + ( hour_p * cos( radians( deg_h ) ) ) ] 
    yh = [ cxy , cxy + ( hour_p * sin( radians( deg_h ) ) ) ] 

    

    return [ [ xh , yh ] , [ xm , ym ] , [ xs , ys ] ] #returning the calculated vars

#-----------------------------------------------------------------

def clock(i):
    res = calculate() #putting the returned vars in a list called "res"
    t = datetime.datetime.now()                                 
    h = t.hour #hour var
    m = t.minute #minute var
    s = t.second #second var

    ax.clear()
    ax.plot( res[2][0] , res[2][1] , color = 'red' , marker = "." ) #drawing the second pointer
    ax.plot( res[1][0] , res[1][1] , color = "black" , marker = "." ) #drawing the minute pointer
    ax.plot( res[0][0] , res[0][1] , color = "blue" , marker = "." ) #drawing the hour pointer 

    x , y , a , b = marker()
    
    ax.plot( a , b , color = "black" , linestyle = "" , marker = "." ) #drawing sope shapes on our clock for easier undrestanding
    ax.plot( x , y , color = "r" , linestyle = "" , marker = "." ) #drawing sope shapes on our clock for easier undrestanding

    plt.scatter( 30 , 30 , color = "black" , s = 50) #drawing a dot in the middle
    ax.add_artist( plt.Circle( ( cxy , cxy ) , 25 , fill = False ) ) #drawing the inner circle
    ax.add_artist( plt.Circle( ( cxy , cxy ) , 27 , fill = False ) ) #drawing the outer circle        
    ax.text( 20 , 17 , "{} : {} : {}".format( h , m , s ) , fontsize = 15 , color = "black") #drawing the analog clock
    ax.set_aspect( "equal" )
    ax.axis( [ 0 , 60 , 0 , 60 ] )

#-----------------------------------------------------------------


anime = animation.FuncAnimation( fig , clock , interval = 1000 )
plt.show()